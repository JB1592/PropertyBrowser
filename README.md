<h1>Description</h1>
<p>The QSI Property Browser is a floating window that can be running in the background during development.  When an Item on the Front Panel or the Block Diagram is selected the values of all of the properties are read. It then allows the developer to view the current values of all the properties and to edit some of the writable properties.  More support for the writable properties is in the works.</p>


The QSI Property Browser is launchable in two ways:
<ol><li>Through the Tools Menu at: Tools->QSI Tools->QSI Property Browser…
<li>Through a Quick Drop Keyboard Shortcut (QDKS) of Ctrl-2</ol>

<h1>LabVIEW Version</h1>
This code is currently published in LabVIEW 2018

<h1>Build Instructions</h1>
Included in the source is the <b>Property Browser.vipb</b> file.  This file is used by the VI Package Manager (VIPM) to build a package that can then be distributed.  Follow the instructions from JKI on how to use VIPM:

<a href="https://vipm.jki.net/">https://vipm.jki.net/</a>

<h1>Installation Guide</h1>
<p>This code is meant to be installed as a VIPM Package.</p>

<p>Manual installation is not recommended but can be accomplished by putting the right files in the right locations within the LabVIEW folder.</p>

<ul><li> All code in the <b>Main Code</b> folder copy to: <i>[LabVIEW]\vi.lib\QSI\Property Browser\Main Code</i>
<li> All code in the <b>All VI Server Class Get Properties</b> folder copy to: <i>[LabVIEW]\vi.lib\QSI\Property Browser\All VI Server Class Get Properties</i>
<li> All code in the <b>Popup Classes</b> folder copy to: <i>[LabVIEW]\vi.lib\QSI\Property Browser\Popup Classes</i>
<li>The <b>Property Browser QD.vi</b> must be copied to: <i>[LabVIEW]\resource\dialog\QuickDrop\plugins</i>
<li>The <b>QSI Property Browser.vi</b> must be copied to: <i>[LabVIEW]\project\QSI Tools</i></ul>

After copying the files the following files will need to be opened, re-linked, and saved:
<ul><li><i>[LabVIEW]\project\QSI Tools\QSI Property Browser.vi</i> (this VI calls the main Format Toolbar.vi located in the vi.lib folder).
<li><i>[LabVIEW]\resource\dialog\QuickDrop\plugins\Property Browser QD.vi</i> (this VI calls the <i>[LabVIEW]\project\QSI Tools\QSI Property Browser.vi</i> file)</ul>

<b>CAUTION: After copying avoid linking back to the source.  It is best to uninstall the Property Browser while working on the source.  Otherwise cross-linking could occur.</b>

<h1>Execution</h1>
After being launched the QSI Property Browser will stay running in the background until it is closed. The developer can carry on programming as normal. 

<h1>Support</h1>
Submit Issues or Merge Requests through GitLab.